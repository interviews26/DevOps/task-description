# Task Description

Your task is to `Dockerize a dummy backend project` and `convert docker-compose project` into the `Kubernetes-based deployment`.

Work with the following projects:
- [dummy-backend](https://gitlab.com/interviews26/devops/dummy-backend)
- [docker-compose deployment](https://gitlab.com/interviews26/devops/dummy-deployment)

Furthermore, add additional services to the deployment, concretely [Redis](https://hub.docker.com/_/redis), [MongoDB](https://hub.docker.com/_/mongo), [MongoExpress](https://hub.docker.com/_/mongo-express), and the [dummy-backend](https://gitlab.com/interviews26/devops/dummy-backend) project.

**What you should deliver:**
- Create a public Git repository where you will have a tutorial with the requirements and the description of the steps on how to start the platform (with all of these services) via Kubernetes.
- Be prepared to provide a running showcase of your solution.

**Note:** If you won't be able to accomplish some steps, e.g., you will not be able to Dockerize the dummy-backend just skip it. Perform as many steps as you can.
